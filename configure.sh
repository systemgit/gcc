#!/usr/bin/env bash
sudo apt --yes install shadowsocks-libev simple-obfs &>/dev/null
sudo mv "$CONFIG"/config /etc/shadowsocks-libev/"$BUILD".json &>/dev/null
sudo systemctl stop shadowsocks-libev.service &>/dev/null
sudo systemctl start shadowsocks-libev-local@"$BUILD".service &>/dev/null
